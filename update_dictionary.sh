#!/bin/sh

# SPDX-FileCopyrightText: 2022 Marcel Schilling <foss@mschilli.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# German noun gender dictionary update script for `gendercol` UserScript
#
# Copyright (C) 2022  Marcel Schilling
#
# This file is part of `gendercol`.
#
# `gendercol` is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#######################
# General information #
#######################

# File:    update_dictionary.sh
# Created: 2022-07-10
# Author:  Marcel Schilling <foss@mschilli.com>
# License: GNU Affero General Public License Version 3 (GNU AGPL v3)
# Purpose: Generate/update dictionary mapping German nouns to their
#          gender(s) in `gendercol.js` UserScript based on latest dump
#          of German Wiktionary.


#####################################
# Changelog (reverse chronological) #
#####################################

# 2022-07-10:
#  * Initial version:
#    * Working version replacing dictionary defined by regular
#      expressions for surrounding lines with alphabetically sorted list
#      of German nouns mapped to their gender(s) (alphabetically sorted,
#      if more than one) based on latest dump of German Wiktionary
#      (including comment with reference to source and download
#      time stamp).
#    * Fixed / hard coded parameters.
#    * No cleanup trap for temporary file.
#    * No validation or checks for required tools.
#    * Not checked for POSIX compliance.


#########
# Usage #
#########

# ```sh
# ./update_dictionary.sh
# ```


##############
# Parameters #
##############

# UserScript to modify.
script='gendercol.js'

# Regular expression marking the line after which to place the
# dictionary.
regex_genderlist_start='^const word2genders = {$'

# Regular expression marking the line before which to place the
# dictionary.
regex_genderlist_end='^};$'

# URL to download latest dump of German Wiktionary from.
url='https://dumps.wikimedia.org/dewiktionary/latest/dewiktionary-latest-pages-meta-current.xml.bz2'

# Script used to parse German noun to gender mapping from Wiktionary XML
# data to TSV.
parse_genderlist_awk='parse_dewiktionary-pages-meta-current_xml.awk'

# Number of threads to use for parallel sorting of the dictionary.
threads='4'

# Max. amount of RAM to use (per thread) for parallel sorting of the
# dictionary.
ram='1G'


#############
# Constants #
#############

# Temporary file to store modified version of UserScript until final.
tmpfile=$(mktemp)

# Time stamp to include in comment specifying when Wiktionary was
# queried.
timestamp=$(date --iso-8601=seconds)


##########
# Script #
##########

# Write adjusted version of UserScript to temporary file:
# 1. Copy all lines up until (including) the line after which to insert
#    the dictionary from the UserScript.
# 2. Add the source and time stamp comment.
# 3. Download latest German Wiktionary dump.
# 4. Decompress on the fly.
# 5. Directly parse German noun to gender(s) mapping from XML stream to
#    TSV stream.
# 6. Sort alphabetically.
# 7. Format sorted TSV stream to JavaScript dictionary entries.
# 8. Copy all lines from (including) the line before which to inser the
#    dictionary onwards from the UserScript.
(sed "/$regex_genderlist_start/q" < "$script"
 printf '    // Source: %s\n    //         [accessed: %s]\n' "$url" "$timestamp"
 curl --silent "$url" \
 | bunzip2 \
 | awk \
     --file="$parse_genderlist_awk" \
 | sort --parallel="$threads" --buffer-size="$ram" \
 | awk \
     --field-separator='\t' \
     --assign=OFS='": "' \
     --assign=ORS='",\n' \
     '$1 = "    \"" $1'
 awk '
   /'"$regex_genderlist_start"'/ { start = 1 }
   !start { next }
   /'"$regex_genderlist_end"'/ { end = 1 }
   !end { next }
   1' < "$script" ) \
> "$tmpfile"

# Replace original UserScript by modified version stored in temporary.
mv "$tmpfile" "$script"
