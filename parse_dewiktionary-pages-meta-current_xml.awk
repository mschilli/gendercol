# SPDX-FileCopyrightText: 2022 Marcel Schilling <foss@mschilli.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# German noun gender dictionary extraction script for `gendercol` UserScript
#
# Copyright (C) 2022  Marcel Schilling
#
# This file is part of `gendercol`.
#
# `gendercol` is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#######################
# General information #
#######################

# File:    parse_dewiktionary-pages-meta-current_xml.awk
# Created: 2022-07-10
# Author:  Marcel Schilling <foss@mschilli.com>
# License: GNU Affero General Public License Version 3 (GNU AGPL v3)
# Purpose: Parse dictionary mapping German nouns to their gender(s) in
#          from XML dump of German Wiktionary to TSV.


#####################################
# Changelog (reverse chronological) #
#####################################

# 2022-07-10:
#  * Initial version:
#    * Working version parsing out nouns and listing all assigned
#      genders in alphabetical order.
#    * Fixed / hard coded parameters.
#    * Some copy/pasted code (particularly regular expressions).
#    * Several parameters constants not defined on top.


#########
# Usage #
#########

# ```sh
# awk -f parse_dewiktionary-pages-meta-current_xml.awk \
#   < input.xml > output.xml
# ```


##############
# Parameters #
##############

BEGIN {

  # Split XML records into lines.
  FS = "\n"

  # Identify records by the page tag.
  RS = " *</[^>]+>" FS " *<page> *" FS

  # Write TSV output.
  OFS = "\t"
}


##########
# Script #
##########

# Skip header.
NR == 1 {
  next
}

# Parse word from title tag in first line of record.
{
  word = $1
  sub(/^ *<title>/, "", word)
  sub(/<\/title>$/, "", word)
}

# Search for first line of record containing text tag.
{
  for (text_begin = 2; text_begin <= NF; ++text_begin) {
    if ($text_begin ~ /<text[> ]/) {
      break
    }
  }
}

# Skip records without text tag (if any).
$text_begin !~ /<text[> ]/ {
  next
}

# Search for first line downstream of (first) title tag line closing the
# title tag.
{
  for (text_end = text_begin; text_end <= NF; ++text_end) {
    if ($text_end ~ /<\/text>/) {
      break
    }
  }
}

# Skip records with unclosed title tag (if any).
$text_end !~ /<\/text>/ {
  next
}

# Search for line declaring the language as German.
{
  # Start with current word to build regular expression.
  lang_regex = word

  # Wrap special characters potentially contained in the word in
  # brackets to escape them.
  # Note: The list of characters that can occur might be incomplete and
  # was determined based on a specific dump of the German Wiktionary at
  # the time of writing of this script.
  gsub(/[\(]/, "[&]", lang_regex)

  # Wrap word into expected prefix(es) and suffix for a valid language
  # line.
  lang_regex = "(^|>)== " lang_regex " [(]{{Sprache[|]Deutsch}}[)] ==$"

  # Actually search for the first language line within the text tag.
  for (lang = text_begin; lang <= text_end; ++lang) {
    if ($lang ~ lang_regex) {
      break
    }
  }
}

# Skip record without a line declaring the language as German (if any).
$lang !~ lang_regex {
  next
}

# Identify all genders (unsorted, including duplicates) of the current
# word if it is annotated to be a noun.
{

  # Initialize gender list as empty.
  genders = ""

  # Search for line(s) declaring the word type as noun within text tag
  # starting at the language line.
  for (type = lang; type <= text_end; ++type) {
    if ($type ~ /^=== {{Wortart[|]Substantiv[|]Deutsch}}, {{[mnf]+}}/) {

      # Parse the gender(s) listed in the current word type line from
      # that line.
      genders_current = $type
      sub(/^=== {{Wortart[|]Substantiv[|]Deutsch}}, {{/, "", genders_current)
      sub(/}}.*$/, "", genders_current)

      # Append current gender(s) to (the end of) the gender list.
      genders = genders "" genders_current
    }
  }
}

# Skip records not annotated as nouns or lacking gender information (if
# any).
!genders {
  next
}

# Generate output TSV record for current German noun:
{
  # Convert gender list to array.
  split(genders, genders_array, "")
  
  # Sort gender array alphabetically.
  asort(genders_array)

  # Re-initialize gender list as empty,
  genders = ""

  # Add genders in alphabetical order skipping over duplicates.
  for(gender in genders_array) {
    gender = genders_array[gender]
    if(!(gender in genders_seen)) {
      genders = genders gender
      genders_seen[gender]
    }
  }
  
  # Clean-up helper array.
  delete genders_seen

  # Print German noun to gender(s) mapping TSV record to STDOUT.
  print word, genders
}
