// SPDX-FileCopyrightText: 2022 Marcel Schilling <foss@mschilli.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// UserScript to color German nouns by gender.
//
// Copyright (C) 2022  Marcel Schilling
//
// This file is part of `gendercol`.
//
// `gendercol` is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.


/*********************\
* General information *
\*********************/

// File:    gendercol.js
// Created: 2022-07-10
// Author:  Marcel Schilling <foss@mschilli.com>
// License: GNU Affero General Public License Version 3 (GNU AGPL v3)
// Purpose: Color German nouns by gender.


/***********************************\
* Changelog (reverse chronological) *
\***********************************/

// 2022-07-10:
//  * Initial version:
//    * Working version with placeholder for gender dictionary.


/*******\
* Usage *
\*******/

// Install UserScript in your web browser, for example using the
// TamperMonkey or GreaseMonkey extensions.


/************\
* Dedication *
\************/

// This is my very first UserScript and the first time I am writing
// anything in JavaScript. I am sharing it with the world out of
// principle but I wrote it for a specific person that also had the idea
// for such a script.
// I am not sure if this person would like to be mentioned by name in a
// public piece of code on the internet, so I'll just say this:
// You know who you are. I hope you like it. Don't try to learn from
// this mess of a script. It might be the ugliest piece of code I've
// ever written. Let's blame JavaScript for that. But I got it to work
// and I'll happily implement any changes based on your feedback.


/********************\
* Greasemonkey header *
\********************/

// ==UserScript==
// @name GenderCol
// @namespace https://mschilli.com/
// @version 0.1.alpha
// @description Color German nouns by gender.
// @require     https://unpkg.com/xregexp/xregexp-all.js
// @match       *://*/*
// @copyright 2022, Marcel Schilling <foss@mschilli.com>
// @license AGPL-3.0-or-later; https://www.gnu.org/licenses/agpl-3.0.html
// ==/UserScript==


/***********\
* Parameters *
\***********/

// Map noun gender(s) to colors.
// Note: German nouns can have several genders (usually changing the
// meaning). In that case, they are assumed to be listed in alphabetical
// order.
const genders2color = {
    "f": "DarkMagenta",
    "m": "MidnightBlue",
    "n": "Peru",
    "fm": "lightpurple",
    "fn": "pink",
    "mn": "darkpurple",
    "fmn": "grey",
};


/***********\
* Constants *
\***********/

// XPath expression to capture all text elements containing upper case
// letters (and thus potentially German nouns).
const xpath_text_with_uppercase = ".//text()[" +
                                         "normalize-space() != '' " +
                                     "and not(ancestor::style) " +
                                     "and not(ancestor::script) " +
                                     "and not(ancestor::textarea) " +
                                     "and not(ancestor::code) " +
                                     "and not(ancestor::pre) " +
                                     "and (    contains(., 'A') " +
                                           "or contains(., 'B') " +
                                           "or contains(., 'C') " +
                                           "or contains(., 'D') " +
                                           "or contains(., 'E') " +
                                           "or contains(., 'F') " +
                                           "or contains(., 'G') " +
                                           "or contains(., 'H') " +
                                           "or contains(., 'I') " +
                                           "or contains(., 'J') " +
                                           "or contains(., 'K') " +
                                           "or contains(., 'L') " +
                                           "or contains(., 'M') " +
                                           "or contains(., 'N') " +
                                           "or contains(., 'O') " +
                                           "or contains(., 'P') " +
                                           "or contains(., 'Q') " +
                                           "or contains(., 'R') " +
                                           "or contains(., 'S') " +
                                           "or contains(., 'T') " +
                                           "or contains(., 'U') " +
                                           "or contains(., 'V') " +
                                           "or contains(., 'W') " +
                                           "or contains(., 'X') " +
                                           "or contains(., 'Y') " +
                                           "or contains(., 'Z') " +
                                           "or contains(., 'Ä') " +
                                           "or contains(., 'Ö') " +
                                           "or contains(., 'Ü') " +
                                  ")]";

// Regular expression capturing word boundaries (unicode-compatible).
// Source: https://stackoverflow.com/a/57290540
const word_boundary_regex = XRegExp("(?<=\\p{L})(?=\\P{L})|(?<=\\P{L})(?=\\p{L})", 'u');


/******************\
* Gender dictionary *
\******************/

// Map German nouns to gender(s).
// Notes:
// * This dictionary is meant to be generated/updated via the
//   `update_dictionary.sh` script provided along this UserScript. For
//   it to work, the initial line (`const word2genders = {`) as well as
//   the final line (`};`) must not be modified.
// * I would have preferred to directly query Wiktionary via its API.
//   However, this requries cross-site requests that are blocked for
//   security reasons. There might be a way to overcome this, but I
//   could not figure it out in the time I had for this little project.
//   Additionally, this way the script will work as well for local web
//   pages in offline mode. While it makes the script huge and required
//   manual updating every once in a while to stay up-to-date with
//   Wiktionary, at least it should perform better relying on a simple
//   look-up in a dictionary / hash table rather than an HTTP request
//   for every uppercase word.
const word2genders = {
    // Run `update_dictionary.sh` to populate this dictionary based on
    // the latest dump of the German Wiktionary.
};


/*******\
* Script *
\*******/

// Note: The general approach to this script was adapted from
// https://stackoverflow.com/a/32537280.

// Capture all text nodes containing upper case letters (and thus
// potentially German nouns).
var text_nodes_with_uppercase = document.evaluate(
  xpath_text_with_uppercase, document.body, null,
  XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null
);

// Iterate over all text nodes containing upper case letters (and thus
// potentially German nouns).
for (nodeIndex = 0; nodeIndex < text_nodes_with_uppercase.snapshotLength;
     ++nodeIndex) {

    // Store current text node containing an upper case letter (and thus
    // potentially a German noun).
    // Note: This node won't be modified directly but rather be replaced
    // by an entirely new fragment. Thus, it can safely be declared as a
    // constant.
    const text_node_with_uppercase = text_nodes_with_uppercase.snapshotItem(
      nodeIndex
    );

    // Skip empty text nodes.
    // Note: This should never happen due to the check for at least one
    // uppercase letter but I decided to keep it from the example script
    // just to be on the safe side. Performance seems good enough to me
    // without any further optimization.
    if (!text_node_with_uppercase.nodeValue) {
        continue;
    }

    // Store parent node of current text node containing an upper case
    // letter (and thus potentially a German noun).
    var parent_node_of_text_node_with_uppercase =
      text_node_with_uppercase.parentNode;

    // Capture all individual words in current text node containing an
    // upper case letter (and thus potentially a German noun).
    const words_in_text_node_with_uppercase =
      XRegExp.split(text_node_with_uppercase.nodeValue, word_boundary_regex);

    // Skip empty text nodes.
    // Note: This should never happen due to the check for at least one
    // uppercase letter but I decided to keep it from the example script
    // just to be on the safe side. Performance seems good enough to me
    // without any further optimization.
    if (!words_in_text_node_with_uppercase) {
        continue;
    }

    // Initialize new fragment to eventually replace current text node
    // containing an upper case letter (and thus potentially a German
    // word).
    var adjusted_fragment = document.createDocumentFragment();

    // Iterate over words in current text node containing an upper case
    // letter (and thus potentially a German noun).
    words_in_text_node_with_uppercase.forEach(function(word, i) {

        // Skip empty words.
        // Note: This might be unnecessary but I decided to keep it from
        // the example script just to be on the safe side. Performance
        // seems good enough to me without any further optimization.
        if (!word.length) {
            return [];
        }

        // Copy over any word that is not a German noun (or whose gender
        // is unknown) to adjusted fragment and exit early.
        // Note: It might be possible to speed things up and/or cause
        // less side-effect by keeping track if this is never triggered
        // for the current text node containing an uppercase letter (but
        // no German noun [with known gender]), and leaving the original
        // text node instead of replacing it with the new fragment built
        // up by effectively copying over the entire text word by word
        // without modification. However, I decided to keep it close to
        // the example script since it works as-is and performance seems
        // to be good enough to me without any further optimization.
        if(!(word in word2genders)) {
           adjusted_fragment.appendChild(document.createTextNode(word));
           return [];
        }

        // Note: This point of the script is only reached for German
        // nouns with known gender.
        
        // Initialize adjusted node with a new span element.
        var adjusted_node = document.createElement('span');

        // Add highlight to modified node depending on the German noun's
        // gender.
        // Note: Instead of changing the text (or background color), I
        // opted for a colored frame to make the script compatible with
        // as many web pages as possible (light and dark mode).
        adjusted_node.style.border = '3px solid ' +
                                      genders2color[word2genders[word]];

        // Copy over German noun to the modified (highlighted) adjusted
        // node.
        adjusted_node.appendChild(document.createTextNode(word));

        // Copy over (highlighted) German noun to adjusted fragment and
        // exit.
        adjusted_fragment.appendChild(adjusted_node);
        return [];
    });

    // Once all (non-empty) words in current text node containing an
    // upper case letter (and thus potentially a German noun) have been
    // copied over (with added highlight for German nouns [with known
    // gender]) to the adjusted fragment, replace them in the parent
    // node.
    // Note: It might be possible to speed things up and/or cause
    // less side-effect by keeping track if the current text node
    // containing an uppercase letter actually contains a German noun
    // (with known gender), and leaving the original text node instead
    // of replacing it with the new fragment built up by effectively
    // copying over the entire text word by word without modification.
    // However, I decided to keep it close to the example script since
    // it works as-is and performance seems to be good enough to me
    // without any further optimization.
    parent_node_of_text_node_with_uppercase.replaceChild(
      adjusted_fragment, text_node_with_uppercase
    );
};
